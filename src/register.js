import React, { Component } from 'react';
import './App.css';
import './login.css';
import './register.css';
import Inputfield from './inputField';



class Register extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
            <h1> Register </h1>
            <br></br>
            <div class="row">
                <div class="column">
                    <Inputfield label="Username"></Inputfield>
                    <br></br>
                    <Inputfield label="Email"></Inputfield>
                    <br></br>
                </div>
                <div class="column">
                    <Inputfield label="Password"></Inputfield>
                    <br></br>
                    <Inputfield label="Confirm Password"></Inputfield>
                </div>
            </div>
        </header>
      </div>
    );
  }
}

export default Register;
