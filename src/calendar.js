import React, { Component } from 'react';
import './App.css';
import './login.css';
import './calendar.css';
import reactlogo from './logo.svg';
import homelogo from './house.svg';
import chatlogo from './chat.png';
import calendarlogo from './calendar.svg';
import maintreqlogo from './maintReq.svg';
import Logo from './Logo.js';
import {homeLogo, messagesLogo, calendarLogo, maintReqLogo} from './Logo.js';


class Calendar extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
            <h2><img src={reactlogo} className="App-logo" alt="logo" />My Landlord<img src={reactlogo} className="App-logo" alt="logo" /></h2>
            <div className="topnav">
                <ul>
                    <li><Logo name={homeLogo.name} logoURL={homelogo} pageURL={homeLogo.pageURL}/></li>
                    <li><Logo name={messagesLogo.name} logoURL={chatlogo} pageURL={messagesLogo.pageURL}/></li>
                    <li><Logo name={calendarLogo.name} logoURL={calendarlogo} pageURL={calendarLogo.pageURL}/></li>
                    <li><Logo name={maintReqLogo.name} logoURL={maintreqlogo} pageURL={maintReqLogo.pageURL}/></li>
                    <div className="topnav-right"><button type="button" onclick="logoutApp()">Log out</button></div>
                </ul>
            </div>
        </header>
        <body>
            <h1>Calendar will go here</h1>
        </body>
      </div>
    );
  }
}

export default Calendar;
