import React, { Component } from 'react';
import reactlogo from './logo.svg';
import homelogo from './house.svg';
import chatlogo from './chat.png';
import calendarlogo from './calendar.svg';
import maintreqlogo from './maintReq.svg';
//import downloadlogo from './download.png';
import './App.css';
import Home from './home';
import Messages from './messages';
import Logo from './Logo.js';
import {homeLogo, messagesLogo, calendarLogo, maintReqLogo} from './Logo.js';
//import sampleLease from './sampleLease.pdf';

class App extends Component {
	constructor(props) {
		super(props)
		this.mainWindowHome = this.mainWindowHome.bind(this)
		this.mainWindowMessages = this.mainWindowMessages.bind(this)
		this.mainWindowCalendar = this.mainWindowCalendar.bind(this)
		this.mainWindowMaintenance = this.mainWindowMaintenance.bind(this)
    this.state = { 
			mainWindow : 0,
    }
  } 
  
  render() {
    return (
      <div className="App">
        <header className="App-header">
					<h2><img src={reactlogo} className="App-logo" alt="logo" />My Landlord<img src={reactlogo} className="App-logo" alt="logo" /></h2>
					<div className="topnav">
						<ul>
							<li><button onClick={this.mainWindowHome}><Logo name={homeLogo.name} logoURL={homelogo} /></button></li>
							<li><button onClick={this.mainWindowMessages}><Logo name={messagesLogo.name} logoURL={chatlogo}/></button></li>
							<li><button onClick={this.mainWindowCalendar}><Logo name={calendarLogo.name} logoURL={calendarlogo}/></button></li>
							<li><button onClick={this.mainWindowMaintenance}><Logo name={maintReqLogo.name} logoURL={maintreqlogo}/></button></li>
							<div className="topnav-right"><button type="button" onclick="logoutApp()">Log out</button></div>
						</ul>
					</div>
        </header>
				<body>
				{this.setMainWindow()}
				</body>
      </div>
    );
	}

	mainWindowHome() {
    this.setState({
      mainWindow : 0,
    })
	 }
	 
	 mainWindowMessages() {
    this.setState({
      mainWindow : 1,
    })
	 }
	 
	 mainWindowCalendar() {
    this.setState({
      mainWindow : 2,
    })
	 }
	 
	 mainWindowMaintenance() {
    this.setState({
      mainWindow : 3,
    })
   }
	
	setMainWindow() {
    if(this.state.mainWindow === 0) {
      return <Home />
    } else if(this.state.mainWindow === 1){
      return <Messages />
    }
  }
}

export default App;
