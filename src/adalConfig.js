import { AuthenticationContext } from 'react-adal';

const adalConfig = {
  tenant: 'CS1530Onegmail.onmicrosoft.com',
 clientId: 'a34e5b0e-0562-4f2a-9e1f-e09bc5eb2c99',
 endpoints: {
  api: 'https://CS1530Onegmail.onmicrosoft.com/a34e5b0e-0562-4f2a-9e1f-e09bc5eb2c99'
},
postLogoutRedirectUri: window.location.origin,
 redirectUri: 'https://mylandlord.azurewebsites.net/',
 //redirectUri: 'https://localhost:3000/',
 cacheLocation: 'localStorage'
};
export const authContext = new AuthenticationContext(adalConfig);