import React, { Component } from 'react';
import './App.css';
import './login.css';

class Logo extends Component {
  render() {
    return(
				<div>
					<img
						src={this.props.logoURL}
						alt={this.props.name}
					/>
				</div>
	);
  }
}

const homeLogo = {
	name: 'Home',
	logoURL : 'https://www.flaticon.com/free-icon/house-black-building-shape_27926#term=home&page=1&position=1',
	pageURL : 'https://mylandlord.azurewebsites.net/',
};

const messagesLogo = {
	name: 'Chat History',
	logoURL : 'https://www.flaticon.com/premium-icon/chat_1155048',
	pageURL : 'https://mylandlord.azurewebsites.net/messages',
};

const calendarLogo = {
	name: 'Calendar',
	logoURL : 'https://www.flaticon.com/free-icon/calendar_1078',
	pageURL : 'https://mylandlord.azurewebsites.net/calendar',
};

const maintReqLogo = {
	name: 'Maintenance Requests',
	logoURL : 'https://www.flaticon.com/free-icon/contract_684831#term=document&page=1&position=1',
	pageURL : 'https://mylandlord.azurewebsites.net/maintenance_requests',
};

export default Logo;
export {homeLogo, messagesLogo, calendarLogo, maintReqLogo};
