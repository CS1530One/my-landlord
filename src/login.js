import React, { Component } from 'react';
import './App.css';
import './login.css'
import Inputfield from './inputField';



class Login extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
            <h1> Login </h1>
            <br></br>
            <div class="row">
                <div class="column">
                    <Inputfield label="Username"></Inputfield>
                </div>
                <div class="column">
                    <Inputfield label="Password"></Inputfield>
                </div>
            </div>
        </header>
      </div>
    );
  }
}

export default Login;
